import glob
import json

from injector import inject

from configuration import Configuration
from domain.videozter import Video


class VideoFileAdapter:

    @inject
    def __init__(self, conf: Configuration):
        self.root_path = conf.root_path

    def convert_to_video(self, file_path):
        video_json = self.load_file_json(file_path)
        return Video(video_json["id"], video_json["display_url"], video_json["__typename"],
                     video_json["taken_at_timestamp"], file_path,
                     video_json["id"])

    def load_files(self):
        videos = glob.glob('{}/video_temp/*.mp4'.format(self.root_path))
        return videos

    def load_videos(self):
        videos = self.load_files()
        return list(map(self.convert_to_video, videos))

    @staticmethod
    def load_file_json(mp4_file_path: str):
        json_file_path = mp4_file_path.replace('.mp4', '.json')
        with open(json_file_path) as json_file:
            data = json.load(json_file)
            return data
