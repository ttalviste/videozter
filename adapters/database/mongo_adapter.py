import injector
import pymongo

from configuration import Configuration
from domain.videozter import Video


class MongoAdapter:

    @injector.inject
    def __init__(self, conf: Configuration):
        client = pymongo.MongoClient(conf.db_connection_string)
        self.videos = client.videozter.videos

    def add_video(self, video: Video):
        saved = 0
        if self.find_by_external_id(video.external_id) is None:
            self.videos.save(video.__dict__)
            saved = 1
        return saved

    def find_by_external_id(self, video_id):
        result = self.videos.find_one(({'external_id': video_id}))
        return result

    @staticmethod
    def convert_to_video(mongo_object):
        return Video(mongo_object["name"], mongo_object["url"], mongo_object["description"],
                     mongo_object["date"], mongo_object["path"],
                     mongo_object["id"])

    def find_all(self):
        return list(map(self.convert_to_video, self.videos.find({})))
