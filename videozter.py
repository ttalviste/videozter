from flask import Flask, jsonify
from logging.config import dictConfig

from flask_injector import FlaskInjector
from injector import inject

from ports.video_service import VideoService
from dependecies import configure

dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default'
    }},
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
})

app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello, World!'


def videos_to_json(video):
    return video.__dict__


@inject
@app.route('/videos')
def get_videos(video_service: VideoService):
    app.logger.info('Running application from %s path', app.root_path)
    videos = video_service.find_all_videos()
    videos_json = list(map(videos_to_json, videos))
    return jsonify(videos_json)


@inject
@app.route('/videos/import')
def import_videos(video_service: VideoService):
    count = video_service.save_all_videos()
    return jsonify('Saved {} videos'.format(count))


FlaskInjector(app=app, modules=[configure])

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80)
