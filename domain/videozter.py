import uuid


class Video:

    def __init__(self, name, url, description, date, path, external_id):
        self.external_id = external_id
        self.path = path
        self.date = int(date)
        self.description = description
        self.url = url
        self.name = name
        self.id = uuid.uuid4()

    def __gt__(self, video2):
        return self.date > video2.date

    def __lt__(self, video2):
        return self.date < video2.date
