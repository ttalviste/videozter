from flask import Flask
from injector import singleton

from configuration import Configuration
from ports.video_service import VideoService
from adapters.database.mongo_adapter import MongoAdapter
from adapters.videos.file_adapter import VideoFileAdapter


def configure(binder):
    binder.bind(Flask, to=Flask(__name__), scope=singleton)
    binder.bind(VideoService, to=VideoService, scope=singleton)
    binder.bind(MongoAdapter, to=MongoAdapter, scope=singleton)
    binder.bind(Configuration, to=Configuration, scope=singleton)
    binder.bind(VideoFileAdapter, to=VideoFileAdapter, scope=singleton)
