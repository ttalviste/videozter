## Test assignment

*Build a data collection and storage backend in python.*

1. Download last month deepfake videos from instagram ([https://github.com/althonos/InstaLooter](https://github.com/althonos/InstaLooter))
instalooter -d --videos-only --time 2020-01-01:2020-02-26 hashtag deepfake output_path
2. Save video files to disk
3. Save them to mongodb with video url, description, date and video file location
4. Create an API with Flask to return these videos based on date. Add unit tests
5. Deploy in it our amazon cloud VM

*Build front end for querying*

1. Page to enter the date for query
2. Page to show the list of results, show title/description, date and instagram url

*Goals:*

1. Clean and understandable code
2. Unit tests

*Evaluation*

1. Evaluate code style
2. Evaluate unit tests