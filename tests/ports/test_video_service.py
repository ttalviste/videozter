from unittest import mock

import pytest
from pytest_mock import mocker

from adapters.database.mongo_adapter import MongoAdapter
from adapters.videos.file_adapter import VideoFileAdapter
from configuration import Configuration
from ports.video_service import VideoService


class TestVideoService:

    @pytest.fixture(scope='function')
    def video_service(self):
        db = MongoAdapter()
        conf = Configuration()
        videos = VideoFileAdapter(conf)
        return VideoService(db, videos)

    def test_should_return_empty_list(self, video_service):
        assert video_service.find_all_videos() == []
