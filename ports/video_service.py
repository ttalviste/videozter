from injector import inject

from adapters.database.mongo_adapter import MongoAdapter
from adapters.videos.file_adapter import VideoFileAdapter
from domain.videozter import Video


class VideoService:

    @inject
    def __init__(self, db_adapter: MongoAdapter, file_adapter: VideoFileAdapter):
        self.file_adapter = file_adapter
        self.db_adapter = db_adapter

    def save_all_videos(self):
        videos = self.file_adapter.load_videos()
        count = 0
        for video in videos:
            result = self.db_adapter.add_video(video)
            count += result
        return count

    def convert_to_video(self, file_path):
        json = self.file_adapter.load_file_json(file_path)
        return Video(json["id"], json["display_url"], json["__typename"], json["taken_at_timestamp"], file_path,
                     json["id"])

    def find_all_videos(self):
        videos = self.db_adapter.find_all()

        return sorted(videos, key=lambda video: video.date)
