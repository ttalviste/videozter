from flask import Flask
from injector import inject


class Configuration:
    @inject
    def __init__(self, app: Flask):
        self.root_path = app.root_path
        self.db_connection_string = "mongodb://tester:testing@127.0.0.1:27017/videozter?retryWrites=true&w=majority"
